var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
var User = require('../user/User');
var VerifyToken = require('./VerifyToken');

var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var config = require('../config');

router.post('/register', function (req, res) {

    var hashedPassword = bcrypt.hashSync(req.body.password, 8);

    User.create({
        name: req.body.name,
        email: req.body.email,
        password: hashedPassword
    },
        function (err, user) {
            if (err) return res.status(500).send('There was a problem registering the user. Error message: ' + err)
            var token = getToken(user);
            res.status(200).send({ auth: true, token: token });
        });

});

router.post('/login', function (req, res) {

    User.findOne({ email: req.body.email }, function (err, user) {
        if (err) return res.status(500).send('Error on the server. Error message: ' + err);
        if (!user) return res.status(404).send('User not found.');

        var passwordIsValid = bcrypt.compareSync(req.body.password, user.password);
        if (!passwordIsValid) return res.status(401).send({ auth: false, token: null });

        var token = getToken(user);

        res.status(200).send({ auth: true, token: token });
    });

});

function getToken(user) {
    return jwt.sign({ id: user._id }, config.secret, {
        expiresIn: 86400
    });
}  

module.exports = router;